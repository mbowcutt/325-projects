#!/bin/sh

echo TIME, WALLTIME, SIZE, REALTIME,

for file in ../data/wget/*.wget;
do
	awk '
		/FINISHED/ {printf substr($2, 3, length($2)) " " substr($3, 0, length($3)-2) ", "}
		/wall/ {printf substr($5, 0, length($5)-1) ", "}
		/Downloaded/ {print substr($4, 0, length($4)-1) ", " substr($6, 0, length($6)-1) ","}' $file
done
