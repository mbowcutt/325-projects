#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <strings.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>

#define PROTOCOL "tcp" // Transport for HTTP

// HTTP Response Strings
#define HTTP_RESPONSE_400 "HTTP/1.1 400 Malformed Request\r\n\r\n"
#define HTTP_RESPONSE_501 "HTTP/1.1 501 Protocol Not Implemented\r\n\r\n"
#define HTTP_RESPONSE_405 "HTTP/1.1 405 Unsupported Method\r\n\r\n"
#define HTTP_RESPONSE_403 "HTTP/1.1 403 Operation Forbidden\r\n\r\n"
#define HTTP_RESPONSE_406 "HTTP/1.1 406 Invalid Filename\r\n\r\n"
#define HTTP_RESPONSE_404 "HTTP/1.1 404 File Not Found\r\n\r\n"
#define HTTP_RESPONSE_200_QUIT "HTTP/1.1 200 Server Shutting Down\r\n\r\n"
#define HTTP_RESPONSE_200_GET "HTTP/1.1 200 OK\r\n\r\n"

// Socket Limits
#define PORT_MIN 1025
#define PORT_MAX 65535

#define QLEN 1

#define BUFLEN 256
#define BITLEN 1
#define BITPOS 0
#define NULLPOS 1

#define EMPTY 0

#define TAG_INDICATOR_POS 0
#define TAG_IDENTIFIER_POS 1

#define ERROR 1
#define SUCCESS 0

#define GET_G_POS 0
#define GET_E_POS 1
#define GET_T_POS 2

#define QUIT_Q_POS 0
#define QUIT_U_POS 1
#define QUIT_I_POS 2
#define QUIT_T_POS 3

int PORTNO; // Listening Port
char * DIRECTORY; // File Serving Directory
char * AUTH_TOKEN; // QUIT ARGUMENT Token
bool VERBOSE = false;

void parse_command (int argc, char *argv[]);    // Parse configuration args
void server_start ();                           // Start the server

char * parse_method (int client);               // Parse request method
char * parse_argument (int client);             // Parse argument method
char * parse_protocol (int client);             // Parse protocol method

bool validate_header (int client);              // Validate request constraints
bool validate_protocol (char protocol[]);       // Validate protocol restraints
bool validate_GET_method (char method[]);       // Validate if GET method
bool validate_QUIT_method (char method[]);      // Validate if QUIT method
bool validate_filename (char argument[]);       // Validate filename constraints
bool validate_token (char argument[]);          // Compare QUIT argument with AUTH Token

void http_respond_400 (int client);             // HTTP 400 Error Response
void http_respond_501 (int client);             // HTTP 501 Error Response
void http_respond_405 (int client);             // HTTP 405 Error Response
void http_respond_403 (int client);             // HTTP 403 Error Response
void http_respond_406 (int client);             // HTTP 406 Error Response
void http_respond_404 (int client);             // HTTP 404 Error Response
void http_respond_200_GET(int client);          // HTTP 200 GET Response
void http_respond_200_QUIT(int client);         // HTTP 200 QUIT Response

void http_respond_GET (int client, char argument[]);  // Respond with 200 GET
bool http_respond_QUIT (int client, char argument[]); // Respond with 200 QUIT 

void parse_command(int argc, char *argv[]) 
{ 
  // Flags for argument conditions 
  bool pFLAG = false; 
  bool dFLAG = false;
  bool aFLAG = false;

  // Loop through command args
  int i = EMPTY; 
  while (i < argc)
    {
      // Check if tag
      char *tag = argv[i++]; 
      if (tag[TAG_INDICATOR_POS] == '-')
 	{
	  // Respond to tag identifier
 	  switch(tag[TAG_IDENTIFIER_POS]) 
 	    { 
 	    case 'p': // Set PORTNO
 	      pFLAG = true; 
 	      PORTNO = atoi(argv[i++]);
	      
 	      if (VERBOSE) 
 		printf("Verbose: Set PORTNO to %i\n", PORTNO); 
	      
 	      break; 
	      
 	    case 'd': // SET DIRECTORY
 	      dFLAG = true; 
 	      DIRECTORY = argv[i++];
	      
 	      if (VERBOSE) 
 		printf("Verbose: Set DIRECTORY to %s\n", DIRECTORY); 
	      
 	      break; 
	      
	    case 'a': // Set AUTH_TOKEN
 	      aFLAG = true;
 	      AUTH_TOKEN = argv[i++];
	      
 	      if (VERBOSE) 
 		printf("Verbose: Set AUTH_TOKEN to %s\n", AUTH_TOKEN);
 	      break; 
	      
 	    case 'v': // Set VERBOSE
 	      VERBOSE = true; 
 	      printf("Verbose: Set VERBOSE true\n"); 
 	      break; 
	      
	    default: // Invalid parameter!
 	      printf("Error: Unexpected argument '%s'\n", tag); 
 	      printf("Usage: -p PORTNO -d DIRECTORY -a AUTH_TOKEN\n"); 
 	      exit(ERROR); 
 	    } 
 	} 
    } 

  // Check that all arguments were parsed
  if (!pFLAG || !dFLAG || !aFLAG) 
    { 
      if (!pFLAG) 
 	printf("Error: No PORTNO\n"); 
      if (!dFLAG) 
 	printf("Error: No DIRECTORY\n"); 
      if (!aFLAG) 
 	printf("Error: No AUTH_TOKEN\n"); 
      printf("Usage: -p PORTNO -d DIRECTORY -a AUTH_TOKEN\n"); 
      exit(ERROR); 
    }

  // Verify Port Number selection
  if (PORTNO < PORT_MIN || PORTNO > PORT_MAX) 
    { 
      printf("Warning: This is not a serious program! Do not select serious ports.\n"); 
      printf("Error: Invalid Port Number\n");
      exit(ERROR); 
    } 
} 

void server_start() 
{
  struct sockaddr_in sin; 
  struct sockaddr addr; 
  struct protoent *protoinfo;
  unsigned int addrlen; 
  int server, client;

  addrlen=sizeof(addr);
  
  // Determine Transport Protocol
  if ((protoinfo = getprotobyname (PROTOCOL)) == NULL) 
    { 
      printf("Error: Could not find protocol information for %s\n", PROTOCOL); 
      exit(ERROR); 
    }
  
  if (VERBOSE) 
    printf("Verbose: Found protocol information for %s\n", PROTOCOL); 
  
  memset ((char *)&sin,0x0,sizeof (sin)); 
  sin.sin_family = AF_INET; 
  sin.sin_addr.s_addr = INADDR_ANY; 
  sin.sin_port = htons(PORTNO); 

  // Create Socket
  server = socket(PF_INET, SOCK_STREAM, protoinfo->p_proto); 
  if (server < SUCCESS) 
    { 
      printf("Error: Error creating socket\n"); 
      exit(ERROR); 
    } 

  if (VERBOSE) 
    printf("Verbose: Created socket\n"); 

  // Bind to port
  if (bind (server, (struct sockaddr *)&sin, sizeof(sin)) < 0) 
    { 
      printf("Error: Error binding to port %i\n", PORTNO); 
       exit(ERROR); 
    }
  
  if (VERBOSE) 
    printf("Verbose: Binded socket to port %i\n", PORTNO); 

  // Listen on port
  if (listen (server, QLEN) < SUCCESS) 
    { 
      printf("Error: Cannot listen on port %i\n", PORTNO); 
      exit(ERROR); 
    } 

  if (VERBOSE) 
    printf("Verbose: Listening for %s connections on port %i...\n", PROTOCOL, PORTNO); 

  // Accept new connections
  bool server_on = true; 
  while(server_on) 
    {
      // Get new connection
      client = accept (server,&addr,&addrlen);
      if (client < SUCCESS) 
	{ 
	  printf("Error: Error while accepting connection\n"); 
	  exit(ERROR); 
	} 
      
      if (VERBOSE) 
	printf("Verbose: New connection\n"); 
      
      // Parse some method, argument, protocol 
      char * method = parse_method(client); 
      char * argument = parse_argument(client); 
      char * protocol = parse_protocol(client);
      
      // Parse remaining HTTP request header 
      if (!validate_header(client)) 
	http_respond_400(client); 
      
      // Validate request parameters 
      else if (!validate_protocol(protocol)) 
	    http_respond_501(client); 

      // Respond to method
      else if (validate_GET_method(method))
	{
	  int pid = fork(); // Write file contents in separate process
	  if (pid == EMPTY)
	    http_respond_GET(client, argument);
	}
      else if (validate_QUIT_method(method))
	server_on = http_respond_QUIT(client, argument); // Determine if to shut down
      else 
	http_respond_405(client);
      close(client);
    } 
  
  if(VERBOSE) 
    printf("Verbose: Server stopped. Exiting...\n"); 
  
  close(server); 
  exit(SUCCESS); 
} 

char * parse_method (int client) 
{ 
  static char method[BUFLEN]; 
  char buf[BITLEN]; 
  bool read_method = true; 
  
  int i = EMPTY; 
  while(read_method) 
    { 
      if (read(client, buf, BITLEN) < SUCCESS) 
 	{ 
 	  printf("Error: Failed to read socket"); 
 	  exit(ERROR); 
 	} 
      
      if (buf[BITPOS] == ' ') 
 	read_method = false;
      else 
 	method[ i++ ] = buf[BITPOS]; 
      
      memset (buf,0x0,BITLEN); 
    } 
  method[ i++ ] = '\0'; 
  
  if (VERBOSE) 
    printf("Verbose: Parsed method as %s\n", method); 
  
  return method; 
} 

char * parse_argument(int client) 
{ 
  char buf[BITLEN]; 
  static char argument[BUFLEN]; 
  bool read_argument = true; 
  
  int i = EMPTY;   
  while(read_argument) 
    { 
      if (read(client, buf, BITLEN) < SUCCESS) 
 	{ 
 	  printf("Error: Failed to read socket"); 
 	  exit(ERROR); 
 	} 
      
      if (buf[BITPOS] == ' ') 
 	read_argument = false; 
      else 
 	argument[ i++ ] = buf[BITPOS];
      
      memset (buf,0x0,BITLEN); 
    }
  argument[ i++ ] = '\0';

  if (VERBOSE) 
    printf("Verbose: Parsed argument as %s\n", argument);
  
  return argument;
}

char * parse_protocol(int client)
{
  char buf[BITLEN];
  static char protocol[BUFLEN];
  bool read_protocol = true;

  int i = EMPTY; 
  while (read_protocol)
    { 
      if (read(client, buf, BITLEN) < SUCCESS) 
 	{ 
 	  printf("Error: Failed to read socket"); 
 	  exit(ERROR); 
 	}
      
      if (buf[BITPOS] == '\r') 
 	{ 
 	  memset (buf,0x0,BITLEN); 
	  
 	  if (read(client, buf, BITLEN) < SUCCESS) 
 	    { 
 	      printf("Error: Failed to read socket"); 
 	      exit(ERROR); 
 	    }
	  
 	  if (buf[BITPOS] == '\n') 
 	    read_protocol = false; 
 	}
      else 
 	protocol[ i++ ] = buf[BITPOS]; 
      
      memset (buf,0x0,BITLEN); 
    } 
  protocol[ i++ ] = '\0'; 
  
  if (VERBOSE) 
    printf("Verbose: Parsed protocol as %s\n", protocol);
  
  return protocol;
}

bool validate_header(int client)
{
  char buf[BITLEN];
  bool read_header = true;
  
  while (read_header)
    {
      if (read(client, buf, BITLEN) < SUCCESS)
	{
	  printf("Error: Failed to read socket");
	  exit(ERROR);
	}
      if (buf[BITPOS] == '\r')
	{
	  memset (buf,0x0,BITLEN);
	  
	  if (read(client, buf, BITLEN) < SUCCESS)
	    {
	      printf("Error: Failed to read socket");
	      exit(ERROR);
	    }
	  if (buf[BITPOS] == '\n')
	    {
	      memset (buf,0x0,BITLEN);
	      
	      if (read(client, buf, BITLEN) < SUCCESS)
		{
		  printf("Error: Failed to read socket");
		  exit(ERROR);
		}
	      if (buf[BITPOS] == '\r')
		{
		  memset (buf,0x0,BITLEN);
		  
		  if (read(client, buf, BITLEN) < SUCCESS)
		    {
		      printf("Error: Failed to read socket");
		      exit(ERROR);
		    }
		  if (buf[BITPOS] == '\n')
		    {
		      if (VERBOSE)
			printf("Verbose: Validated rest of HTTP request header\n");
		      return true;
		    }
		  else
		    return false;
		}
	      else
		continue;
	    }
	  else
	    return false;
	}  
    }
}

bool validate_protocol(char protocol[])
{
  int i = EMPTY;
  char * prot = "HTTP/";
  while (i < strlen(prot))
    {
      if (protocol[i] != prot[i++])
	{
	  if (VERBOSE)
	    printf("Error: Could not validate protocol as 'HTTP/'\n");
	  
	  return false;
	}
    }
  
  if (VERBOSE)
    printf("Verbose: Validated 'HTTP/' protocol\n");
      
  return true;
}

bool validate_GET_method(char method[])
{
  if ( method[GET_G_POS] == 'G' &&
       method[GET_E_POS] == 'E' &&
       method[GET_T_POS] == 'T' )
    {
      if (VERBOSE)
	printf("Verbose: Validated HTTP method as 'GET'\n");
      
      return true;
    }
  else
    {
      if (VERBOSE)
	printf("Verbose: Could not validate HTTP method as 'GET'\n");
      
      return false;
    }
}

bool validate_QUIT_method(char method[])
{
  if ( method[QUIT_Q_POS] == 'Q' &&
       method[QUIT_U_POS] == 'U' &&
       method[QUIT_I_POS] == 'I' &&
       method[QUIT_T_POS] == 'T')
    {
      if (VERBOSE)
	printf("Verbose: Validated HTTP method as 'QUIT'\n");
      
      return true;
    }
  else
    {
      if (VERBOSE)
	printf("Verbose: Could not validate HTTP method as 'QUIT'\n");
      
      return false;
    }
}

void http_respond_400 (int client)
{
  if (write(client, HTTP_RESPONSE_400, strlen(HTTP_RESPONSE_400)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 400 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 400 response\n");
}

void http_respond_501 (int client)
{
  if (write(client, HTTP_RESPONSE_501, strlen(HTTP_RESPONSE_501)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 501 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 501 response\n");
}

void http_respond_405 (int client)
{
  if (write(client, HTTP_RESPONSE_405, strlen(HTTP_RESPONSE_405)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 405 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 405 response\n");
}

void http_respond_403 (int client)
{
  if (write(client, HTTP_RESPONSE_403, strlen(HTTP_RESPONSE_403)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 403 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 403 response\n");
}

void http_respond_406 (int client)
{
  if (write(client, HTTP_RESPONSE_406, strlen(HTTP_RESPONSE_406)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 406 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 406 response\n");
}

void http_respond_404 (int client)
{
  if (write(client, HTTP_RESPONSE_404, strlen(HTTP_RESPONSE_404)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 404 response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 404 response\n");
}

void http_respond_200_GET (int client)
{
  if (write(client, HTTP_RESPONSE_200_GET, strlen(HTTP_RESPONSE_200_GET)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 200 GET response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 200 GET response\n");
}

void http_respond_200_QUIT (int client)
{
  if (write(client, HTTP_RESPONSE_200_QUIT, strlen(HTTP_RESPONSE_200_QUIT)) < SUCCESS)
    {
      printf("Error: Error while returning HTTP 200 QUIT response\n");
      exit(ERROR);
    }
  
  if (VERBOSE)
    printf("Verbose: Server returned HTTP 200 QUIT response\n");
}

void http_respond_GET(int client, char argument[])
{
  if (VERBOSE)
    printf("Verbose: Responding with GET protocol\n");
  
  if (!validate_filename(argument))
    http_respond_406(client);
  else
    {
      char location[BUFLEN];
      sprintf(location, "%s%s", DIRECTORY, argument);
      
      if (VERBOSE)
	printf("Verbose: Requested document is at %s\n", location);
      
      FILE* file = fopen(location, "r");
      if (!file)
	http_respond_404(client);  
      
      else
	{
	  if (VERBOSE)
	    printf("Verbose: Opened document reader stream\n");
	  
	  http_respond_200_GET(client);
	  
	  char buf[BUFLEN];
	  while (fgets(buf, BUFLEN, file))
	    {
	      if (write(client, buf, strlen(buf)) < SUCCESS)
		{
		  printf("Error: Error while sending file contents\n");
		  exit(ERROR);
		}
	      memset (buf,0x0,BUFLEN);
	    }
	  
	  if (VERBOSE)
	    printf("Verbose: Finished writing file\n");

	  fclose(file);
	  close(client);
	  exit(SUCCESS);
	}
    }
}

bool http_respond_QUIT(int client, char argument[])
{
  if (VERBOSE)
    printf("Verbose: Responding with QUIT protocol\n");
  
  if (validate_token(argument))
    {
      if (VERBOSE)
	printf("Verbose: Token match\n");
      
      http_respond_200_QUIT(client);
      return false;
    }
  else
    {
      if (VERBOSE)
	printf("Verbose: Token mismatch\n");
      
      http_respond_403(client);
      return true;
    }
}

bool validate_filename (char argument[])
{
  if (argument[BITPOS] != '/')
    {
      if (VERBOSE)
	printf("Verbose: Parsed filename is invalid\n");
      
      return false;
    }
  else if (argument[NULLPOS] == '\0')
    {
      if (VERBOSE)
	{
	  printf("Verbose: Parsed filename is /, using /default.html\n"); 
	}
      
      sprintf(argument, "/default.html");
      return true;
    }
  else
    {
      if (VERBOSE)
	printf("Verbose: Parsed filename is valid\n");
      
      return true;
    }
}

bool validate_token (char argument[])
{
  if (strlen(argument) != strlen(AUTH_TOKEN))
    {
      if (VERBOSE)
	printf("Verbose: Argument length does not match token\n");
	  
      return false;
    }

  else
    {
      int i = EMPTY;
      while (i < strlen(argument))
	{
	  if (argument[i] != AUTH_TOKEN[i++])
	    {
	      if (VERBOSE)
		printf("Verbose: Argument character does not match token\n");
	      
	      return false;
	    }
	}

      if (VERBOSE)
	printf("Verbose: No mismatch found\n");
	  
      return true;
    }
}

int main(int argc, char *argv [])
{
  parse_command(argc, argv);
  server_start();
}
