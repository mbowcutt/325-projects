#!/bin/sh

echo TIME, 200 OK, 301 MOVED, 403 FORBIDDEN, 404 NOT FOUND, 500 SERVER ERROR,

for file in ../data/wget/*.wget
do
	gawk '
		/FINISHED/ {printf substr($2, 3, length($2)) " " substr($3, 0, length($3)-2) ", "}
		/request/	{ 
				if ($6 == "200")
					++OK
				else if ($6 == "301")
					++MOVED
				else if ($6 == "403")
					++FORBIDDEN
				else if ($6 == "404")
					++NOTFOUND
				else if ($6 == "500")
					++SERVERERROR
				else print $6
				}
			END { print OK ", " MOVED ", " FORBIDDEN ", " NOTFOUND ", " SERVERERROR "," }' $file
done
