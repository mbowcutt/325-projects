/* Michael Bowcutt
 * mwb71@case.edu
 * proj2.c
 * 10/1/18
 * 
 * This file is a web client as specified
 * in the Project 2 announcement.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define ERROR 1     // Error constant used as exit codes
#define SUCCESS 0   // Success constant used as exit code
#define EMPTY 0     // Constant used to identify empty strings and arrays
#define FAIL -1     // Fail constant recieved from function calls
#define PROTOCOL "tcp" // Default transport protocol
#define HTTP_PORT 80  // Default HTTP port
#define BUFLEN 512    // Base Size used for arrays and buffers
#define CAPITAL_LETTER_MIN 65  // lowest int value of capital chars ('A')
#define CAPITAL_LETTER_MAX 90  // highest int value of capitacl chars ('Z')
#define CAPITAL_LOWERCASE_OFFSET 32 // Capital-lowercase char int difference
#define PROTOCOL_POS 7  // Number of characters in url dedicated to HTTP:// protocol
#define FIRST_POS 0
#define SECOND_POS 1
#define READLEN 1

int errexit (char *format, char *arg);  // Exit with code and message
char * parse_request(int argc, char *argv[]); // Takes command args and returns HTTP request
void parseURL(char * url, char * hostname, int * port, char * web_filename); // Parses the hostname, port, and filename from -u tag
char * getRequestMessage(char * web_filename, char * hostname, int port); // constructs the requestmessage
void printDetail(char * hostname, int port, char * web_filename, char * output_filename); // prints details about our command call
int connect_socket(); // Connects us to the host server and returns a socket descriptor
void requestHTTP(int sd, char * request); // Sends the request text to the socket and reads the response

int port = HTTP_PORT;         // Set port to default value
char hostname[BUFLEN];        // create buffer for hostname
char web_filename[BUFLEN];    // create buffer for web filename
char *output_filename;        // buffer for host filename
char requestMessage[BUFLEN];  // buffer for request message
bool _output, _detail, _request, _response; // flags used to determine output

int errexit (char *format, char *arg)
{
    fprintf (stderr,format,arg);
    fprintf (stderr,"\n");
    exit (ERROR);
}

char * parse_request(int argc, char *argv[])
{
  // Loop through all args
  int i = EMPTY;
  while (i < argc)
    {
      // Check for tags, denoted with the '-' char
      char *tag = argv[i++];
      if (tag[0] == '-')
	{
	  // Perform necessary tasks based on tag
	  switch(tag[1])
	    {
	      // [REQUIRED]
	      // -u specify URL
	    case 'u':
	      parseURL(argv[i++], hostname, &port, web_filename);	      
	      break;

	      // [REQUIRED]
	      // -o specify file name
	    case 'o':
	      _output = true;
	      output_filename = argv[i++];
	      break;
	      
	      // [OPTIONAL]
	      // -d print command details
	    case 'd':
	      _detail = true;
	      break;

	      // [OPTIONAL]
	      // -r print HTTP request
	    case 'r':
	      _request = true;
	      break;

	      // [OPTIONAL]
	      // -R print HTTP response
	    case 'R':
	      _response = true;
	      break;
	      
	      // [EXTRA CREDIT]
	      // -f follow redirects
	    case 'f':
	      printf("-f Selected \n");
	      break;

	      // [EXTRA CREDIT]
	      // -c chunked encoding
	    case 'c':
	      printf("-c Selected \n");
	      break;

	      // Unknown Tag
	    default:
	      errexit("No match for argument: %s", argv[i]);
	      break;
	    }
	}
    }

  // Error if no URL or bad hostname
  if (strlen(hostname) == EMPTY)
    errexit("Parse Error: %s", "No hostname specified");

  // Error if no output file
  if (!_output)
    errexit("Parse Error: %s", "No output file specified");

  // Print command details if necessary
  if (_detail)
    printDetail(hostname, port, web_filename, output_filename);

  // Return formatted HTTP request message
  return getRequestMessage(web_filename, hostname, port);
}

char * getRequestMessage(char * web_filename, char * hostname, int port)
{
  char methodLine[BUFLEN];
  char hostLine[BUFLEN];
  char userAgentLine[BUFLEN];

  sprintf(methodLine, "GET %s HTTP/1.0\r\n", web_filename);
  if (port != HTTP_PORT)
    sprintf(hostLine, "Host: %s:%i\r\n", hostname, port);
  else
    sprintf(hostLine, "Host: %s\r\n", hostname);
  sprintf(userAgentLine, "User-Agent: MWB71 EECS325 CLIENT 0.1\r\n");

  // Print request if necessary
  if (_request)
    {
      printf("REQ: %s", methodLine);
      printf("REQ: %s", hostLine);
      printf("REQ: %s", userAgentLine);
    }

  // Concatenate messages
  sprintf(requestMessage, "%s%s%s\r\n", methodLine, hostLine, userAgentLine);
  return requestMessage;
}

void parseURL(char * url, char * hostname, int * port, char * web_filename)
{
  // Check for HTTP protocol
  char protocol[PROTOCOL_POS];
  strncpy (protocol, url, PROTOCOL_POS); // copy first seven chars plus one for null
  //  protocol[PROTOCOL_POS - 1]= '\0';
  
  // make lowercase if capital
  int i;
  for ( i = EMPTY; i < strlen(protocol); i++ )
    {
      if (protocol[i] >= CAPITAL_LETTER_MIN &&
	  protocol[i] <= CAPITAL_LETTER_MAX)
	protocol[i] = protocol[i] + CAPITAL_LOWERCASE_OFFSET;
    }

  // err if not http
  if (strcmp(protocol, "http://") != SUCCESS)
    errexit("ERROR: http:// protocol expected, found: %s", protocol);

  // Loop through remaining string
  for (i = PROTOCOL_POS; i <= strlen(url) ; i++)
    {
      // If we hit ':' we should note the custom port before checking file
      if (url[i] == ':')
	{
	  i++; // ignore ':'
	  int port_pos = i;
	  char port_str[BUFLEN];

	  // Read until file
	  while (url[i] != '/')
	    {
	      port_str[i - port_pos] = url[i];
	      i++;
	    }
	  
	  // Finish port string
	  *port = atoi(port_str); // Save as int

	  // Read file
	  int file_pos = i;
	  while (i < strlen(url))
	    {
	      web_filename[i - file_pos] = url[i];
	      i++;
	    }
	  // If no file, find /
	  if (strlen(web_filename) == EMPTY)
	    web_filename[FIRST_POS] = '/';
	  web_filename[strlen(web_filename)]='\0';
	  break;
	}
      
      else if (url[i] == '/' || url[i] == '\0')
	{
	  int file_pos = i;
	  while (i < strlen(url))
	    {
	      web_filename[i - file_pos] = url[i];
	      i++;
	    }
	  if (strlen(web_filename) == EMPTY)
	    web_filename[FIRST_POS] = '/';
	  break;
	}

      else
	{
	  hostname[i - PROTOCOL_POS] = url[i];
	}
    }
}

void printDetail(char * hostname, int port, char * web_filename, char * output_filename)
{
  printf("DET: hostname = %s\n", hostname);
  printf("DET: port = %i\n", port);
  printf("DET: web_filename = %s\n", web_filename);
  printf("DET: output_filename = %s\n", output_filename); 
}

int connect_socket()
{
  struct sockaddr_in sin;
  struct hostent *hinfo;
  struct protoent *protoinfo;
  int sd;

  /* lookup the hostname */
  hinfo = gethostbyname(hostname);
  if (hinfo == NULL)
    errexit ("cannot find name: %s", hostname);
  
  /* set endpoint information */
  memset ((char *)&sin, 0x0, sizeof (sin));
  sin.sin_family = AF_INET;
  sin.sin_port = htons (port);
  memcpy ((char *)&sin.sin_addr,hinfo->h_addr,hinfo->h_length);

  if ((protoinfo = getprotobyname (PROTOCOL)) == NULL)
    errexit ("cannot find protocol information for %s", PROTOCOL);
  
  /* allocate a socket */
  /*   would be SOCK_DGRAM for UDP */
  sd = socket(PF_INET, SOCK_STREAM, protoinfo->p_proto);
  if (sd < SUCCESS)
    errexit("cannot create socket",NULL);
  
  /* connect the socket */
  if (connect (sd, (struct sockaddr *)&sin, sizeof(sin)) < SUCCESS)
    errexit ("cannot connect", NULL);

   return sd;
}

void requestHTTP(int sd, char * request)
{
  // Send the HTTP Request
  if (send(sd, request, strlen(request), EMPTY) < SUCCESS)
    errexit("sending error", NULL);

  int ret;
  char buffer[BUFLEN];
  bool headerflag = true; // Tells us if we're reading the a header
  bool headerstart = true; // Tells us if we're starting a header arg
  bool _ok = NULL; // Tells us if the response is 200 OK

  FILE *file = fopen(output_filename, "w");
  if (file == NULL)
    errexit("Error opening output file.", NULL);

  // Read until complete
  memset (buffer,0x0,BUFLEN);
  while(read (sd, buffer, READLEN) >= SUCCESS && strlen(buffer) > EMPTY)
    {
      // If we're reading the response and it's OK, save it to file
      if (!headerflag && _ok)
	{
	  if (buffer[FIRST_POS] == '%')
	    buffer[SECOND_POS] = '%';
	  fprintf(file, buffer);
	  memset (buffer,0x0,BUFLEN);
	}

      // If we're reading the header, it's the start of an arg and we are supposed to print it, append `RSP: ` before printing
      if (headerflag && headerstart && _response)
	{
	  printf("RSP: ");
	  headerstart = false;
	}

      // print the header if we need to and are reading a header
      if (headerflag && _response)
	printf("%s", buffer);

      // Check the response code if we haven't already: 200 OK
      if (!_ok && headerflag && buffer[FIRST_POS] == '2')
	{
	  memset(buffer,0x0,BUFLEN);
	  if (read (sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '0')
	    {
	      if(_response)
		printf("%s",buffer);
	      if (read (sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '0')
		{
		  if(_response)
		    printf("%s",buffer);
		  _ok = true;
		}
	    }
	  else
	    {
	      if(_response)
		printf("%s",buffer);
	    }
	}

      /* check if FOLLOW REDIRECT
      else if (!_ok && headerflag && buffer[FIRST_POS] == '3')
	{
	  memset(buffer,0x0,BUFLEN);
	  if (read (sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '0')
	    {
	      if(_response)
		printf("%s",buffer);
	      if (read (sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '1')
		{
		  if(_response)
		    printf("%s",buffer);
		  _ok = true;

		  // FOLLOW REDIRECT
		  
		}
	    }
	  else
	    {
	      if(_response)
		printf("%s",buffer);
	    }
	}
      */

      // Check for line end
      else if (headerflag && buffer[FIRST_POS] == '\r')
	{
	  memset (buffer,0x0,BUFLEN);
	  if (read (sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '\n')
	    {
	      if(_response)
		printf("%s", buffer);
	      headerstart = true;
	      memset (buffer,0x0,BUFLEN);
	      
	      if (read(sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '\r')
		{
		  if(_response)
		    printf("%s", buffer);
		  memset (buffer,0x0,BUFLEN);
		  if (read(sd, buffer, READLEN) >= SUCCESS && buffer[FIRST_POS] == '\n')
		    {
		      if(_response)
			printf("%s", buffer);
		      memset (buffer,0x0,BUFLEN);
		      headerflag = false;
		    }
		  else
		    {
		      if (_response)
			printf("%s", buffer);
		    }
		}
	      else
		{
		  if (_response)
		    printf("RSP: %s", buffer);
		  headerstart = false;
		}
	    }
	  else
	    {
	      if (_response)
		printf("%s", buffer);
	    }
	}
      memset (buffer,0x0,BUFLEN);
    } 
}

int main (int argc, char *argv [])
{
  char *request = parse_request(argc, argv);
  int sd = connect_socket();
  requestHTTP(sd, request);
  exit(SUCCESS);
}
