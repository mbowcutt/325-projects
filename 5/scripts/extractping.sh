#!/bin/sh

output_filename="../data/csv/ping.csv"
       
> $output_filename
for file in ../data/ping/*.ping;
do
    data="$(awk '/rtt/ {print $4}' $file)"
    echo $data | awk '{printf $1}' FS=/ >> $output_filename
    echo -n ", " >> $output_filename
    echo $data | awk '{printf $2}' FS=/ >> $output_filename
    echo -n ", " >> $output_filename
    echo $data | awk '{printf $3}' FS=/ >> $output_filename
    echo ", " >> $output_filename
done
