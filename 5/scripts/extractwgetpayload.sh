#!/bin/sh

echo application/html, image/vnd.microsoft.icon, text/plain, image/png, application/octet-stream, font/woff, font/woff2, application/font-woff, image/gif, text/css, application/javascript, image/jpeg,

for file in ../data/wget/*.wget;
do
	awk '
		/Length/	{
				if ($4 =="[text/html]")
					html=html + $2
				else if ($4 =="[image/vnd.microsoft.icon]")
					microsoft=microsoft + $2
				else if ($4 =="[image/x-icon]")
					xicon=xicon + $2
				else if ($4 =="[text/plain]")
					plain=plain + $2
				else if ($4 =="[image/png]")
					png=png + $2
				else if ($4 =="[application/octet-stream]")
					octet_stream = octet_stream + $2
				else if ($4 =="[font/woff]")
					woff=woff + $2
				else if ($4 =="[font/woff2]")
					woff2=woff2 + $2
				else if ($4 =="[application/font-woff]")
					font_woff = font_woff + $2
				else if ($4 =="[image/gif]")
					gif=gif + $2
				else if ($4 =="[text/css]")
					css=css + $2
				else if ($4 =="[application/javascript]")
					javascript=javascript + $2
				else if ($4 =="[image/jpeg]")
					jpeg=jpeg + $2
				else if ($3 =="[text/html]")
					html=html + $2
				else if ($3 =="[image/vnd.microsoft.icon]")
					microsoft=microsoft + $2
				else if ($3 =="[image/x-icon]")
					xicon=xicon + $2
				else if ($3 =="[text/plain]")
					plain=plain + $2
				else if ($3 =="[image/png]")
					png=png + $2
				else if ($3 =="[application/octet-stream]")
					octet_stream = octet_stream + $2
				else if ($3 =="[font/woff]")
					woff=woff + $2
				else if ($3 =="[font/woff2]")
					woff2=woff2 + $2
				else if ($3 =="[application/font-woff]")
					font_woff = font_woff + $2
				else if ($3 =="[image/gif]")
					gif=gif + $2
				else if ($3 =="[text/css]")
					css=css + $2
				else if ($3 =="[application/javascript]")
					javascript=javascript + $2
				else if ($3 =="[image/jpeg]")
					jpeg=jpeg + $2
				else print $0


			}
		END {print html ", " microsoft ", " xicon ", " plain ", " png ", " octet_stream ", " woff ", " woff2 ", " font_woff ", " gif ", " css ", " 
javascript ", " jpeg ","}' $file
done
