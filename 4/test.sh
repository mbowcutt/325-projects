#!/bin/bash

big_message(){
    echo "###########################"
    echo $1
    echo "###########################"
}

test_trace(){
    echo -n -e "$1: "
    diff -q "samples/$1$2.out" <(./proj4 -t samples/$1.trace $2) && echo "PASS"
}

test_trace_sorted(){
    echo -n -e "$1: "
    diff -q "samples/$1$2.out" <(./proj4 -t samples/$1.trace $2 | sort -n) && echo "PASS"
}

summary_tests() {
    big_message "SUMMARY REPORT"
    echo
    test_trace low -s
    test_trace shout -s
    test_trace refugee -s
    test_trace thunder -s
    test_trace 409 -s
    echo
}

length_tests() {
    big_message "LENGTH ANALYSIS REPORT"
    echo
    test_trace low -l
    test_trace shout -l
    test_trace alive -l
    test_trace thunder -l
    test_trace kashmir -l
    echo
}

printing_tests() {
    big_message "PACKET PRINTING REPORT"
    echo
    test_trace low -p
    test_trace shout -p
    test_trace thunder -p
    test_trace kashmir -p
    test_trace truckin -p
    echo
}

matrix_tests() {
    big_message "TRAFFIC MATRIX REPORT"
    echo
    test_trace_sorted low -m
    test_trace_sorted shout -m
    test_trace_sorted thunder -m
    test_trace_sorted kashmir -m
    echo
}

summary_tests
length_tests
printing_tests
matrix_tests
