/* EECS 325 - Computer Networks - Project 4
 * Case Western Reserve University
 * Professor Mark Allman <mallman@case.edu>
 * Fall 2018
 *
 * Michael Bowcutt <mbowcutt@case.edu>
 * 11/14/2018
 */

#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <inttypes.h>
#include <limits.h>

#include </usr/include/net/ethernet.h>
#include </usr/include/netinet/ip.h>
#include </usr/include/netinet/udp.h>
#include </usr/include/netinet/tcp.h>
#include </usr/include/netinet/in.h>
#include <arpa/inet.h>

#define MAX_PKT_SIZE 1500

#define SUCCESS 0
#define ERROR 1

#define READ_ERROR 0
#define READ_SUCCESS 1

#define RETURN_ERROR 1
#define RETURN_SUCCESS 0

#define TAGPOS 0
#define FLAGPOS 1
#define DEBPOS 2

#define EMPTY 0

#define BITLEN 1
#define NIBLEN 4

#define TABLE_SIZE 255

// verbose debugging flags
bool DEBUG_PHYS = false;
bool DEBUG_LINK = false;
bool DEBUG_NET = false;
bool DEBUG_TRANS = false;
bool DEBUG_APP = false;

// program exit wrapper
void errexit(char *err)
{
  printf("Error: %s\n", err);
  exit(ERROR);
}

struct traffic_vector
{
  uint saddr;
  uint daddr;
  int payload;
  struct traffic_vector *next;
  bool full;
};

/* record of information nabout the current packet */
struct pkt_info
{
  unsigned short caplen;
  int now_secs;
  int now_usecs;
  unsigned char pkt [MAX_PKT_SIZE];
  struct ether_header *ethh;  /* ptr to ethernet header, if fully present */
  struct iphdr *iph;          /* ptr to IP header, if fully present */
  struct tcphdr *tcph;        /* ptr to TCP header, if fully present */
  struct udphdr *udph;        /* ptr to UDP header, if fully present */
};

/* meta information, using same layout as trace file */
struct meta_info
{
    unsigned short caplen;
    unsigned short ignored;
    unsigned int secs;
    unsigned int usecs;
};

struct meta_info meta;       // meta info
struct ether_header ethh;    // ethernet info
struct iphdr iph;            // IP info
struct tcphdr tcph;          // TCP info
struct udphdr udph;          // UDP info

int pkt, ip_pkts;            // counters

int bytes_read;              // read return
int ether_type;              // ethernet type (we check for IP)
int ip_len, iphl;            // IP packet and header length
char *protocol;              // transport protocol type
int trans_hl, payload_len; // tansport header and payload length

struct traffic_vector matrix[TABLE_SIZE];

// loop through packet
unsigned short next_packet (FILE * fd, struct pkt_info *pinfo)
{
  // clear previous information
  memset (pinfo,0x0,sizeof (*pinfo));
  memset (&meta,0x0,sizeof (meta));

  // read meta information
  bytes_read = fread (&meta, 1, sizeof (meta), fd);
  if (bytes_read == READ_ERROR)
    return READ_ERROR;

  // increment packet and save info
  pkt++;
  pinfo->caplen = ntohs (meta.caplen);  // save caplen
  pinfo->now_secs = ntohl(meta.secs);   // save seconds since epoch
  pinfo->now_usecs = ntohl(meta.usecs); // save microseconds since epoch

  // print layer 1 verbose debug info
  if (DEBUG_PHYS)
    {
      printf("PHYSICAL LAYER DEBUG - PACKET %d\n", pkt);
      printf("********************\n");
      printf("TS: \t\t%d.%d\n", pinfo->now_secs, pinfo->now_usecs);
      printf("CAPLEN: \t%d\n", pinfo->caplen);
      printf("\n");
    }

  // continue if caplen is empty
  if (pinfo->caplen == EMPTY)
    return (RETURN_ERROR);

  // exit program if caplen too big
  if (pinfo->caplen > MAX_PKT_SIZE)
    errexit("caplen too large");

  // read incomplete ethernet header
  if (pinfo->caplen < sizeof(ethh))
    {
      bytes_read = fread(&ethh, BITLEN, pinfo->caplen, fd);
      if (bytes_read == READ_ERROR)
	return READ_ERROR;

      if (DEBUG_LINK)
	{
	  printf("LINK LAYER DEBUG - PACKET %d\n", pkt);
	  printf("***INCOMPLETE***\n");
	  printf("SIZE: \t\t%d\n", pinfo->caplen);
	  printf("\n");
	}
    }

  // read ethernet header
  else
    {
      memset (&ethh,0x0,sizeof(ethh));
      bytes_read = fread (&ethh, BITLEN, sizeof (ethh), fd);
      pinfo->ethh = &ethh;
      ether_type = ntohs(ethh.ether_type);

      // increment if IP packet
      if (ether_type == ETHERTYPE_IP)
	ip_pkts++;
      
      // print debug info
      if (DEBUG_LINK)
	{
	  printf("LINK LAYER DEBUG - PACKET %d\n", pkt);
	  printf("****************\n");
	  printf("SIZE: \t\t%d\n", sizeof(ethh));
	  printf("ETHER_TYPE: \t%d\n", ether_type);
	  printf("\n");
	}

      // read incomplete IP header
      if(pinfo->caplen - sizeof(ethh) < sizeof(iph))
	{
	  memset (&iph,0x0,sizeof (iph));
	  bytes_read = fread(&iph, BITLEN, pinfo->caplen - sizeof(ethh), fd);
	  
	  if(DEBUG_NET)
	    {
	      printf("NETWORK LAYER DEBUG - PACKET %d\n", pkt);
	      printf("****INCOMPLETE*****\n");
	      printf("SIZE: \t\t%d\n", pinfo->caplen - sizeof(ethh));
	      printf("\n");
	    }
	}

      // read IP header
      else
	{
	  memset (&iph,0x0,sizeof (iph));
	  bytes_read = fread (&iph, BITLEN, sizeof (iph), fd);
	  pinfo->iph = &iph;

	  // get IP packet length
	  ip_len = ntohs(iph.tot_len);

	  // get IP header length
	  iphl = iph.ihl*NIBLEN;

	  if(DEBUG_NET)
	    {
	      printf("NETWORK LAYER DEBUG - PACKET %d\n", pkt);
	      printf("*******************\n");
	      printf("SIZE: \t\t%d\n", sizeof(iph));
	      printf("HEADER LENGTH: \t%d\n", iphl);
	      printf("PACKET LENGTH: \t%d\n", ip_len);
	      printf("PROTOCOL: \t%d\n", iph.protocol);
	      printf("\n");
	    }

	  // find transport protocol
	  switch(iph.protocol)
	    {
	      // TCP
	    case IPPROTO_TCP:
	      protocol = "T";

	      // read incomplete TCP header
	      if (pinfo->caplen - (sizeof(ethh) + sizeof(iph)) < sizeof(tcph))
		{
		  memset (&tcph, 0x0, sizeof(tcph));
		  bytes_read = fread(&tcph, BITLEN, pinfo->caplen - (sizeof(ethh) + sizeof(iph)), fd);

		  if(DEBUG_TRANS)
		    {
		      printf("TRANSPORT LAYER DEBUG - PACKET %d\n");
		      printf("**********INCOMPLETE**********\n");
		      printf("PROTOCOL: %s\n", protocol);
		      printf("SIZE: %d\n", pinfo->caplen - (sizeof(ethh) + sizeof(iph)));
		      printf("\n");
		    }
		  fflush(stdout);
		}

	      // read TCP header
	      else
		{
		  memset (&tcph,0x0,sizeof (tcph));
		  bytes_read = fread(&tcph, BITLEN, sizeof(tcph), fd);
		  pinfo->tcph = &tcph;
		  
		  trans_hl = tcph.th_off * NIBLEN;
		  payload_len = ip_len - iphl - trans_hl;

		  // TCP Options
		  if (trans_hl > sizeof(tcph))
		    {
		      struct tcphdr tcpopt;

		      // incomplete TCP options
		      if (pinfo->caplen - (sizeof(ethh) + sizeof(iph) + sizeof(tcph)) < trans_hl - sizeof(tcph))
			{
			  bytes_read = fread(&tcpopt, BITLEN, pinfo->caplen - (sizeof(ethh) + sizeof(iph) + sizeof(tcph)), fd);
			  
			}

		      // read TCP options
		      else
			bytes_read = fread(&tcpopt, BITLEN, (trans_hl - sizeof(tcph)), fd);
		    }
		  
		  if (DEBUG_TRANS)
		    {
		      printf("TRANSPORT LAYER DEBUG - PACKET %d\n", pkt);
		      printf("******************************\n");
		      printf("SIZE: %d\n", sizeof(tcph));
		      printf("PROTOCOL: %s\n", protocol);
		      printf("HEADER LENGTH: %d\n", trans_hl);
		      printf("PAYLOAD LENGTH: %d\n", payload_len);
		      printf("\n");
		    }
		  fflush(stdout);
		}
	      break;

	      // UDP
	    case IPPROTO_UDP:
	      protocol = "U";
	      
	      if(pinfo->caplen - (sizeof(ethh) + sizeof(iph)) < sizeof(udph))
		{
		  memset (&udph,0x0,sizeof (udph));
		  bytes_read = fread(&udph, BITLEN, pinfo->caplen - (sizeof(ethh) + sizeof(iph)), fd);

		  if(DEBUG_TRANS)
		    {
		      printf("TRANSPORT LAYER DEBUG - PACKET %d\n");
		      printf("**********INCOMPLETE**********\n");
		      printf("PROTOCOL: %s\n", protocol);
		      printf("SIZE: %d\n", pinfo->caplen - (sizeof(ethh) + sizeof(iph)));
		      printf("\n");
		    }
		  fflush(stdout);
		}
	      else
		{
		  memset (&udph,0x0,sizeof (udph));
		  bytes_read = fread(&udph, BITLEN, sizeof(udph), fd);
		  pinfo->udph = &udph;
		  
		  // trans_hl = ntohs(udph.uh_ulen);
		  trans_hl = sizeof(udph);
		  payload_len = ip_len - iphl - trans_hl;

		  if (DEBUG_TRANS)
		    {
		      printf("TRANSPORT LAYER DEBUG - PACKET %d\n", pkt);
		      printf("******************************\n");
		      printf("SIZE: %d\n", sizeof(tcph));
		      printf("PROTOCOL: %s\n", protocol);
		      printf("HEADER LENGTH: %d\n", trans_hl);
		      printf("PAYLOAD LENGTH: %d\n", payload_len);
		      printf("\n");
		    }
		  fflush(stdout);
		}
	      break;
	      
	    default:
	      protocol = "?";
	      bytes_read = fread(&udph, BITLEN, pinfo->caplen - (sizeof(ethh) + sizeof (iph)), fd);

	      if (DEBUG_TRANS)
		    {
		      printf("TRANSPORT LAYER DEBUG - PACKET %d\n", pkt);
		      printf("******************************\n");
		      printf("SIZE: %d\n", sizeof(tcph));
		      printf("PROTOCOL: %s\n", protocol);
		      printf("\n");
		    }
	      fflush(stdout);
	      break;
	    }	  	  
	} // read IP header	  
    }  
  return READ_SUCCESS;
}

int main(int argc, char *argv[]){
  char * filename;
  bool summary = false;
  bool length_analysis = false;
  bool packet_printing = false;
  bool traffic_matrix = false;
  
  int i = EMPTY;
  
  while (i < argc)
    {
      char * arg = argv[ i++ ];
      if (arg[TAGPOS] == '-')
	{
	  switch(arg[FLAGPOS])
	    {
	    case 't':
	      filename = argv[ i++ ];
	      break;

	    case 's':
	      summary = true;
	      if (length_analysis ||
		  packet_printing ||
		  traffic_matrix)
		errexit("selected more than one mode");
	      break;

	    case 'l':
	      length_analysis = true;
	      if (summary ||
		  packet_printing ||
		  traffic_matrix)
		errexit("selected more than one mode");
	      break;

	    case 'p':
	      packet_printing = true;
	      if (length_analysis ||
		  summary ||
		  traffic_matrix)
		errexit("selected more than one mode");
	      break;

	    case 'm':
	      traffic_matrix = true;
	      if (length_analysis ||
		  packet_printing ||
		  summary)
		errexit("selected more than one mode");
	      break;

	    case '-': // verbose debugging
	      switch(arg[DEBPOS])
		{
		case 'p':
		  DEBUG_PHYS = true;
		  break;
	
		case 'l':
		  DEBUG_LINK = true;
		  break;

		case 'n':
		  DEBUG_NET = true;
		  break;

		case 't':
		  DEBUG_TRANS = true;
		  break;
		  
		case 'a':
		  DEBUG_APP = true;
		  break;

		default: //error
		  errexit("debug not found");
		  break;
		}
	      break;
	    default:
	      errexit("unexpected argument");
	    }
	}    
    }
  
  static int first_secs, first_usecs, last_secs, last_usecs;
  struct pkt_info pinfo;
  memset (matrix, EMPTY, sizeof(matrix));
  
  FILE * fd;
  fd = fopen (filename, "rb");
  if (fd < SUCCESS)
    errexit("could not open file");
  
  while (next_packet (fd, &pinfo))
    {
      if (summary)
	{	  
	   if (first_secs == EMPTY || pinfo.now_secs < first_secs ||
	      (pinfo.now_secs == first_secs &&
	       pinfo.now_usecs < first_usecs))
	    {
	      first_secs = pinfo.now_secs;
	      first_usecs = pinfo.now_usecs;
	    }
	  if (pinfo.now_secs > last_secs ||
	      (pinfo.now_secs == last_secs &&
	       pinfo.now_usecs > last_usecs))
	    {
	      last_secs = pinfo.now_secs;
	      last_usecs = pinfo.now_usecs;
	    }
	} // summary
      
      else if (length_analysis)
	{
	  // print time and caplen
	  printf("%d.%06d %d ", 
		 pinfo.now_secs, pinfo.now_usecs,
		 pinfo.caplen);

	  // print non IP packet
	  if(ether_type != ETHERTYPE_IP)
	    printf("- - - - -\n");
	    
	  // print incomplete IP packet
	  else if (pinfo.caplen < (sizeof(ethh) + sizeof(iph)))
	    printf("- - - - -\n");

	  // print IP information
	  else
	    {
	      printf("%d %d %s ", 
		     ip_len,
		     iphl,
		     protocol);

	      // print unknown transport 
	      if (protocol == "?")
		printf("? ?\n");

	      // check if incomplete TCP
	      else if (protocol == "T" &&
		       pinfo.caplen < (sizeof(ethh) +
				       iphl +
				       trans_hl))
		printf("- -\n");

	      // check if incomplete UDP
	      else if (protocol == "U" &&
		       pinfo.caplen < (sizeof(ethh) +
				       iphl +
				       trans_hl))
		printf("- -\n");

	      // print transport information
	      else
		printf("%d %d\n",
			   trans_hl,
			   payload_len);
	    }
	} // length analysis

      else if (packet_printing && protocol == "T"
	       && pinfo.caplen >= sizeof(ethh) + iphl + trans_hl)
	{
	  
	  printf("%u.%06u ", pinfo.now_secs, pinfo.now_usecs);
	  
	  struct in_addr ipaddr;
	  ipaddr.s_addr = iph.saddr;
	  
	  printf("%s %u ", inet_ntoa(ipaddr), ntohs(tcph.source));
	  
	  ipaddr.s_addr = iph.daddr;
	  printf("%s %u ", inet_ntoa(ipaddr), ntohs(tcph.dest));

	  printf("%u %u %u ",
		 iph.ttl,
		 ntohs(tcph.th_win),
		 ntohl(tcph.th_seq));
	  
	  if (tcph.ack)
	    printf("%u\n", ntohl(tcph.th_ack));
	  else
	    printf("-\n");

	  fflush(stdout);
	}
      
      else if (traffic_matrix && protocol == "T"
	       && pinfo.caplen >= sizeof(ethh) + iphl + trans_hl)
	{
	  const uint saddr = iph.saddr;
	  const uint daddr = iph.daddr;
	  struct in_addr ipaddr;
	  
	  int slot = saddr % TABLE_SIZE;
	  struct traffic_vector vector;
	  vector = matrix[slot];

	  if (DEBUG_APP)
	    {
	      ipaddr.s_addr = saddr;
	      printf("SLOT %d: Analyzing from %s ",slot, inet_ntoa(ipaddr));
	      fflush(stdout);
	      ipaddr.s_addr = daddr;
	      printf("to %s for %d bits...\n", inet_ntoa(ipaddr), payload_len);
	      fflush(stdout);
	    }
	  
	  if (!vector.full) // if empty, make new vector
	    {
	      if (DEBUG_APP)
		printf("SLOT %d: empty. Adding new record\n", slot);

	      struct traffic_vector *vec = malloc(sizeof(struct traffic_vector));
	      vec->saddr = saddr;
	      vec->daddr = daddr;
	      vec->payload = payload_len;
	      vec->full = true;
	      vec->next = NULL;
	      matrix[slot] = *vec;
	      vector = matrix[slot];

	      if (DEBUG_APP)
		printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);
	     
	    }

	  // otherwise, if the source and destination are different,
	  // we must search for the right vector in this slot
	  else if (vector.saddr != saddr || vector.daddr != daddr)
	    {
	      if(DEBUG_APP)
		{
		  ipaddr.s_addr = saddr;
		  printf("SLOT %d: NEW VECTOR: Analyzing from %s ",slot, inet_ntoa(ipaddr));
		  fflush(stdout);
		  ipaddr.s_addr = daddr;
		  printf("to %s for %d bits...\n", inet_ntoa(ipaddr), payload_len);
		  fflush(stdout);
		}

	      // Next slot is empty
	      if (vector.next == NULL)
		{
		  if(DEBUG_APP)
		    printf("Next slot empty. Adding new record\n");

		  struct traffic_vector *vec = malloc(sizeof(struct traffic_vector));
		  vec->saddr = saddr;
		  vec->daddr = daddr;
		  vec->payload = payload_len;
		  vec->full = true;
		  vec->next = NULL;
		  vector.next = vec;

		  if (DEBUG_APP)
		    printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);

		}

	      // Next slot not empty, walk down chain
	      else if (vector.next == NULL)
		{
		  // loop until next is empty or a match
		  while (!vector.next->full &&
			 (vector.next->saddr != saddr ||
			  vector.next->daddr != daddr))
		    {
		      
		      if (DEBUG_APP)
			printf("SLOT %d: not empty and destinations didn't match. Getting next slot..\n", slot);
		      
		      vector = *vector.next;
		      
		      if (DEBUG_APP)
			printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);
		      
		    }

		  // now if the next vector is empty, make a new vector
		  if (vector.next == NULL)
		    {
		      if (DEBUG_APP)
			printf("Next slot empty. Adding new record\n");
		      
		      struct traffic_vector *vec = malloc(sizeof(struct traffic_vector));
		      vec->saddr = saddr;
		      vec->daddr = daddr;
		      vec->payload = payload_len;
		      vec->full = true;
		      vec->next = NULL;
		      vector.next = vec;
		      
		      if(DEBUG_APP)
			printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);
		      
		    }
		  
		  // otherwise, the source and destinion match set next slot
		  else
		    {
		      if(DEBUG_APP)
			printf("Found appropriate slot. Updating next..\n");

		      vector.next->payload += payload_len;
		      
		      if(DEBUG_APP)
			printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);
		    }
		}
	    }
	  else
	    {
	      if(DEBUG_APP)
		printf("Slot matched. Updating current slot.\n");

	      vector.payload = vector.payload + payload_len;
	      matrix[slot] = vector;

	      if(DEBUG_APP)
		printf("vector source: %d\nvector destination: %d\nvector payload:%d\n", vector.saddr, vector.daddr, vector.payload);
	    }
	}
    }

  fclose (fd);

  if (summary)
    {
      printf("TIME SPAN: %d.%d - %d.%d\n",
	     first_secs,
	     first_usecs,
	     last_secs,
	     last_usecs);
      printf("TOTAL PACKETS: %i\n", pkt);
      printf("IP PACKETS: %i\n", ip_pkts);
    }

  else if (traffic_matrix)
    {
      // traffic matrix
      struct traffic_vector vector;
      for (int i = EMPTY; i < TABLE_SIZE; i++)
	{
	  vector = matrix[i];
	  if (vector.full)
	    {
	      struct in_addr ipaddr;
	      ipaddr.s_addr = vector.saddr;
	      printf("%s ", inet_ntoa(ipaddr));
	      fflush(stdout);
	      ipaddr.s_addr = vector.daddr;
	      printf("%s ", inet_ntoa(ipaddr));
	      fflush(stdout);
	      printf("%d\n", vector.payload);

	      //	      printf("%d\n", vector.next);
	      if (vector.next == NULL)
		continue;
	      
	      while (vector.next->full)
		{
		  vector = *vector.next;
		  ipaddr.s_addr = vector.saddr;
		  printf("%s ", inet_ntoa(ipaddr));
		  fflush(stdout);
		  ipaddr.s_addr = vector.daddr;
		  printf("%s ", inet_ntoa(ipaddr));
		  fflush(stdout);
		  printf("%d\n", vector.payload);
		  // printf("%d %d %d\n", vector.saddr, vector.daddr, vector.payload);
		  //	      fflush(stdout);
		  // vector = *vector.next;
		}
	    }
	}
    }
  
  return SUCCESS;
}
